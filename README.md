## Programmation Java 
### Contenus proposés par Mme ABDELMOULA
2022-2023

Site de référence : <https://frebourg.es/java/>

## Notions parcourues 

- [x] Pensée computationnelle
- [x] Langage Java, Outils d'édition, compilation et exécution : Programme "HelloWorld"
- [x] La variable (concept et type)
- [x] Les entrées et sorties (System.in et System.out)

Evaluation intermédiaire 

- [x] Structure conditionnelle (if, if else, if else if else)
- [ ] Structure répétitive conditionnelle (while)
