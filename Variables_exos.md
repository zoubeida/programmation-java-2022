## Variables en Java
### Exercices 
Mme ABDELMOULA 13/09/2022


### Manipuler les types de base
1. Quels sont les nombres correctement écrits en langage Java ?
    
    a. 3,14 
    
    b. -175 
    
    c. 2,718 
    
    d. 1.975 
    
    e. 1 290 524

2. Quels sont les nombres de type `int` ?
    
    a. 1.975 
    
    b. .175 
    
    c. -5 
    
    d. 1.E10 
    
    e. 123456 
    
    f. -17.

3. Quelles sont les chaînes de caractères qui sont correctement écrites en Java ?
    
    a. "Java, c'est facile ! "
    
    b. 'Il dit qu'il fait beau.'
    
    c. "Il dit : "Il faut beau"."

4. Quels sont les noms de variable corrects en Java ?
    
    a. tAiLLe 
    
    b. _secret 
    
    c. email@ 
    
    d. i+j 
    
    e. nXm

    f. @email 
    
    g. 2Pi 
    
    h. _2pi 
    
    i. pi2
